require('ts-node/register')

exports.config = {
  baseUrl: 'http://127.0.0.1:3000',
  seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
  specs: [
    'test/*.spec.ts'
  ],
  framework: 'jasmine2',
  allScriptsTimeout: 110000,
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: ['show-fps-counter=true'],
    },
  },
  directConnect: true,
  onPrepare: function() {
    browser.ignoreSynchronization = true
  },
  useAllAngular2AppRoots: true,
}
