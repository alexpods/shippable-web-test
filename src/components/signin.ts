import { Component } from '@angular/core'

@Component({
  selector: 'teachme-signin',
  template: require('./signin.html'),
  styles: [require('./signin.scss')],
})
export class SigninComponent {}