import { Component, Input } from '@angular/core'
import { FormControl } from '@angular/forms'

@Component({
  selector: 'teachme-input',
  template: require('./input.html')
})
export class InputComponent {
  @Input() type: string
  @Input() placeholder: string
  @Input() control: FormControl
  @Input() errors: any

  get error(): string {
    if (!this.errors) {
      return null
    }

    for (let name in this.errors) {
      if (this.control.hasError(name)) {
        return this.errors[name]
      }
    }

    return null
  }
}
