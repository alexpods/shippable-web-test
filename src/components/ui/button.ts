import { Directive, Optional } from '@angular/core'
import { ControlContainer, FormGroup, FormControl, FormArray, AbstractControl } from '@angular/forms'

@Directive({
  selector: 'button',
  host: {
    '(click)': 'onClick($event)',
    '[disabled]': 'isDisabled'
  }
})
export class ButtonDirective {

  constructor(
    @Optional() private _controlContainer: ControlContainer
  ) {}

  get isDisabled(): boolean {
    return Boolean(
      this._controlContainer
      &&
      this._checkAllControlsAreDirty(this._controlContainer.control)
      &&
      this._controlContainer.invalid
    )
  }

  onClick(): void {
    if (this._controlContainer && !this._controlContainer.dirty) {
      this._markAsDirty(this._controlContainer.control)
    }
  }

  private _markAsDirty(control: AbstractControl): void {
    if (control instanceof FormGroup) {
      for (const controlName in control.controls) {
        this._markAsDirty(control.controls[controlName])
      }
    }

    if (control instanceof FormArray) {
      for (const controlIndex in control.controls) {
        this._markAsDirty(control.controls[controlIndex])
      }
    }

    control.markAsDirty()
  }

  private _checkAllControlsAreDirty(control: AbstractControl): boolean {
    if (control instanceof FormGroup) {
      for (const controlName in control.controls) {
        if (!control.controls[controlName].dirty) {
          return false
        }
      }
    }

    if (control instanceof FormArray) {
      for (const controlIndex in control.controls) {
        if (!control.controls[controlIndex].dirty) {
          return false
        }
      }
    }

    return control.dirty
  }
}