import { Component } from '@angular/core'

@Component({
  selector: 'teachme-app',
  template: require('./app.html'),
  styles:   [require('./app.scss')],
})
export class AppComponent {}
