import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { validateEmail } from '../validators/email'
import { Api } from '../services/api'

@Component({
  selector: 'teachme-join',
  template: require('./join.html'),
  styles: [require('./join.scss')],
})
export class JoinComponent implements OnInit {

  newUserForm: FormGroup

  constructor(
    private _formBuilder: FormBuilder,
    private _api: Api
  ) {}

  ngOnInit() {
    this.newUserForm = this._formBuilder.group({
      displayName: ['', [
        Validators.required,
      ]],
      email:       ['', [
        Validators.required,
        validateEmail,
      ]],
      password:    ['', [
        Validators.required,
        Validators.minLength(5)
      ]],
    })
  }

  joinWithFacebook(): void {
    console.log('join with facebook')
  }

  joinWithGooglePlus(): void {
    console.log('join with google plus')
  }

  joinWithVkontakte(): void {
    console.log('join with vkontakte')
  }

  joinWithTwitter(): void {
    console.log('join with twitter')
  }

  joinByProvidingPersonalInformation(): void {
    if (this.newUserForm.valid) {
      const displayName = this.newUserForm.get('displayName').value
      const email       = this.newUserForm.get('email').value
      const password    = this.newUserForm.get('password').value

      this._api.createNewUser(displayName, email, password)
    }
  }
}