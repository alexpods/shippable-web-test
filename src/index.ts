import './vendor.scss'
import './vendor.ts'

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'
import { NgModule } from '@angular/core'
import { APP_BASE_HREF } from '@angular/common'
import { BrowserModule } from '@angular/platform-browser'
import { HttpModule } from '@angular/http'
import { ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'

import { AppComponent } from './components/app'
import { SigninComponent } from './components/signin'
import { JoinComponent } from './components/join'

import { InputComponent } from './components/ui/input'
import { ButtonDirective } from './components/ui/button'

import { Api } from './services/api'

@NgModule({
  bootstrap: [
    AppComponent
  ],
  declarations: [
    AppComponent,
    SigninComponent,
    JoinComponent,
    InputComponent,
    ButtonDirective,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/join', pathMatch: 'full' },
      { path: 'join',   component: JoinComponent   },
      { path: 'signin', component: SigninComponent },
    ])
  ],
  providers: [
    Api,
    { provide: APP_BASE_HREF, useValue: '/' },
  ]
})
export class Module {}


platformBrowserDynamic().bootstrapModule(Module).catch((error) => {
  console.error(error)
})
