FROM node:7.3.0
MAINTAINER Alexey Podskrebyshev <alexey.podskrebysehv@gmail.com>
RUN mkdir -p /app
WORKDIR /app

COPY npm-shrinkwrap.json .
RUN npm install

COPY . .

RUN npm run build

ENTRYPOINT ["npm", "run"]
CMD ["start"]
