const webpack = require('webpack')
const path = require('path')
const fs = require('fs')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin')
const DllPlugin = require('webpack/lib/DllPlugin')
const DllReferencePlugin = require('webpack/lib/DllReferencePlugin')
const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin')

const HOST = '0.0.0.0'
const PORT = 80

const VENDOR_NAME = 'vendor'
const INDEX_NAME   = 'index'

const ROOT_PATH = path.resolve(__dirname)
const DIST_PATH = path.resolve(ROOT_PATH, 'dist')
const SRC_PATH  = path.resolve(ROOT_PATH, 'src')

const VENDOR_SCRIPTS_PATH      = path.resolve(SRC_PATH, 'vendor.ts')
const VENDOR_STYLES_PATH       = path.resolve(SRC_PATH, 'vendor.scss')
const VENDOR_DLL_MANIFEST_PATH = path.resolve(DIST_PATH, VENDOR_NAME + '-manifest.json')
const INDEX_SCRIPTS_PATH       = path.resolve(SRC_PATH, 'index.ts')
const INDEX_STYLES_PATH        = path.resolve(SRC_PATH, 'index.scss')
const INDEX_HTML_PATH          = path.resolve(SRC_PATH, 'index.html')
const ICONS_PATH               = path.resolve(SRC_PATH, 'icons')

const ICONS_PATHS = fs.readdirSync(ICONS_PATH)
  .map(filename => /\.svg$/.test(filename) && path.resolve(ICONS_PATH, filename))
  .filter(v => v)

const vendorConfig = {
  entry: [
    VENDOR_SCRIPTS_PATH,
    VENDOR_STYLES_PATH,
  ],
  output: {
    path:               DIST_PATH,
    filename:          `${VENDOR_NAME}.js`,
    sourceMapFilename: `${VENDOR_NAME}.map`,
    chunkFilename:     `[id].chunk.js`,
    library:           `${VENDOR_NAME}`,
    libraryTarget:     `var`,
  },
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader', 'sass-resources-loader'],
      }
    ],
  },
  plugins: [
    new DllPlugin({
      name: VENDOR_NAME,
      path: VENDOR_DLL_MANIFEST_PATH
    }),
    new LoaderOptionsPlugin({
      options: {
        sassResources: [
          INDEX_STYLES_PATH
        ]
      }
    })
  ],
}

const mainConfig = {
  entry: [
    ...ICONS_PATHS,
    INDEX_STYLES_PATH,
    INDEX_SCRIPTS_PATH,
  ],
  output: {
    path:              DIST_PATH,
    filename:          `${INDEX_NAME}.js`,
    sourceMapFilename: `${INDEX_NAME}.map`,
    chunkFilename:     `[id].chunk.js`,
    library:           `${INDEX_NAME}`,
    libraryTarget:     `var`,
  },
  resolve: {
    extensions: ['.ts', '.js', '.json']
  },
  module: {
    loaders:  [
      {
        test: /\.ts$/,
        loaders: ['awesome-typescript-loader'],
      },
      {
        test: /\.html$/,
        loaders: ['raw-loader'],
      },
      {
        test: /\.scss$/,
        loaders: ['raw-loader', 'postcss-loader', 'sass-loader', 'sass-resources-loader']
      },
      {
        test: /\.svg$/,
        loaders: ['svg-sprite-loader', 'svgo-loader'],
      }
    ]
  },
  plugins: [
    new ContextReplacementPlugin(
      /angular(\\|\/)core(\\|\/)src(\\|\/)linker/,
      SRC_PATH
    ),
    new HtmlWebpackPlugin({
      template: INDEX_HTML_PATH,
      filename: 'index.html',
      inject: false,
    }),
    new DllReferencePlugin({
      context: ROOT_PATH,
      sourceType: 'var',
      get manifest() {
        return require(VENDOR_DLL_MANIFEST_PATH)
      }
    }),
    new LoaderOptionsPlugin({
      options: {
        sassResources: [
          INDEX_STYLES_PATH
        ],
        postcss: [
          require('autoprefixer')
        ]
      }
    })
  ],
  devServer: {
    contentBase: DIST_PATH,
    host: HOST,
    port: PORT,
    historyApiFallback: true,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000,
    }
  }
}

switch (process.env.BUNDLE) {
  case 'VENDOR':
    module.exports = vendorConfig
    break
  case 'INDEX':
    module.exports = mainConfig
    break
  default:
    module.exports = mainConfig
}
